<?php
/*
Plugin Name: WH Post Link Shortcode
Description: A shortcode to show the post link by slug
Author: Webhead LLC
Version: 0.1
License: GPL2
*/


/**
 * [postlink anchor_text="anchor text" slug="foo-value"]
 * text - The text for the link
 * slug - will get the first published post with this slug.
 */
function postlink_func( $atts ) {
	extract( shortcode_atts( array(
	    'anchor_text' => '',
		'text' => '',
		'slug' => '',
	), $atts ) );
	$args = array('numberposts'=>1, 'post_type'=>'any');
	$link = '';
	if (strlen($slug) != 0) {
	    $args['name'] = $slug;
	    $posts = get_posts($args);
	    if ($posts) {
	        $post = $posts[0];
	        $link = get_permalink($post->ID);
	        if (strlen($anchor_text) == 0) {
	            $anchor_text = get_the_title($post->ID);
	        }
	    }
	}
	return sprintf('<a href="%s">%s</a>', $link, $anchor_text);
}
add_shortcode( 'postlink', 'postlink_func' );